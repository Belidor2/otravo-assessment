> This repo contains recipe for testing tour booking scenario using Cypress

## Installation

```bash
## clone this project
https://gitlab.com/belidor/otravo-assessment.git
cd ./otravo-assessment
## install all dependencies
npm install
```

## Opening Cypress GUI

```bash
# and open Cypress GUI
npx cypress open
```

## Running from the CLI

Same as running Cypress GUI but with `cypress run` command (and any CLI arguments)

```bash
### runs all example projects in specific browser
### similar to cypress run --browser <name>
npx cypress run --browser chrome
