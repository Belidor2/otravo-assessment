const passengersInfo = require('../../fixtures/passengers-info')

describe('Tour booking scenario', () => {
		it('Compare the price before check-out with the final price', () => {
			// Setting Cookie for avoiding Privacy Agreement notification
			cy.setCookie('cookieConsent', 'all')

			// Open BaseUrl
			cy.visit('/')

			// Open sun vertical
			cy.get('article.vertical.sun > a > button').click()

			// Open the destination selector
			cy.get('#select-destination').click()

			// Select Spanje
			cy.contains('Spanje').click()

			// Open date
			// Using .wait() to render date selector closing
			cy.get('.departure [data-cy="calendar-date-selector"]').click().wait(500)

			// Open month selector
			cy.get('.current-month').click()

			// Select future month
			cy.get('.future-months > a:nth-child(4)').click()

			// Select date
			cy.get('.dates > :nth-child(17)').click()

			// Open duration selector
			cy.get('#select-duration').click()

			// Select duration
			cy.contains('11-16 dagen').click()

			// Travel party should be 2 person
			cy.get('.travel-party').click()
			cy.get('.party-adults select').select('2')

			// Click Search button
			cy.get('button.box-submit').click({force: true})

			// Check "Incl.vlucht" to be active
			cy.contains('Incl. vlucht').should('have.class', 'active')

			// Select Schiphol airport
			cy.contains('Amsterdam (Schiphol)').click()

			// Select All Inclusive
			cy.contains('All inclusive').click()

			//Check the filters
			cy.get('.search-facets .icon-flag').should('contain.text', 'Spanje')
			cy.get('.search-facets > :nth-child(2)').should('have.text', 'vlucht')
			cy.get('.search-facets > :nth-child(3)').should('have.text', 'zonvakantie')
			cy.get('.search-facets > :nth-child(4)').should('have.text', 'Amsterdam (Schiphol)')
			cy.get('.search-facets > :nth-child(5)').should('have.text', 'All inclusive')

			// Choose the first accomodation
			cy.get('.search-results-list li:nth-child(1) .btn').click()

			// Check the price
			cy.get('[data-cy="price-check-button"]').click()
			cy.get(':nth-child(4) > .price > .price-number').then((price) => {
				// Because price element contain "," symbol
				var checkOutPrice = parseFloat(price.text())

				// Book
				cy.get('#offer-receipt-bookbutton').should('exist')
				cy.get('#offer-receipt-bookbutton').click()

				// Fill first passenger info
				cy.get('[data-cy=passenger-01_name-salutation-M]').click()
				cy.get('[data-cy=passenger-01_name-first]').type(passengersInfo["passenger-1"]["name"])
				cy.get('[data-cy=passenger-01_name-last]').type(passengersInfo["passenger-1"]["surname"])
				cy.get('.passenger-01 > .form-group > .input-birthdate__day > select').select(passengersInfo["passenger-1"]["birth-date-day"])
				cy.get('.passenger-01 > .form-group > .input-birthdate__month > select').select(passengersInfo["passenger-1"]["birth-date-month"])
				cy.get('.passenger-01 > .form-group > .input-birthdate__year > select').select(passengersInfo["passenger-1"]["birth-date-year"])
				cy.get('.passenger-01 .icon-flag').contains(passengersInfo["passenger-1"]['nationality'])

				// Fill second passenger info
				cy.get('[data-cy=passenger-02_name-salutation-F]').click()
				cy.get('[data-cy=passenger-02_name-first]').type(passengersInfo["passenger-2"]["name"])
				cy.get('[data-cy=passenger-02_name-last]').type(passengersInfo["passenger-2"]["surname"])
				cy.get('.passenger-02 > .form-group > .input-birthdate__day > select').select(passengersInfo["passenger-2"]["birth-date-day"])
				cy.get('.passenger-02 > .form-group > .input-birthdate__month > select').select(passengersInfo["passenger-2"]["birth-date-month"])
				cy.get('.passenger-02 > .form-group > .input-birthdate__year > select').select(passengersInfo["passenger-2"]["birth-date-year"])
				cy.get('.passenger-02 .icon-flag').contains(passengersInfo["passenger-2"]['nationality'])


				// Fill main booker info
				cy.get('[data-cy=passenger-01_email]').type(passengersInfo["main-booker"]["email"])
				cy.get('[data-cy=passenger-01_address-postcode]').type(passengersInfo["main-booker"]["postcode"])
				cy.get('[data-cy=passenger-01_address-number]').type(passengersInfo["main-booker"]["buildind"])
				cy.get('[data-cy=passenger-01_telephone-primary]').type(passengersInfo["main-booker"]["phone"])
				cy.get('[data-cy=passenger-01_address-street]').should('have.value', passengersInfo["main-booker"]["street"])
				cy.get('[data-cy=passenger-01_address-city]').should('have.value', passengersInfo["main-booker"]["city"])
				cy.get('.passenger-01-contact-info .icon-flag').contains(passengersInfo["main-booker"]['land'])


				// Fill emergency info
				cy.get('[data-cy=stay-home_name]').type(passengersInfo["emergency-contact"]["name"])
				cy.get('[data-cy=stay-home_tel]').type(passengersInfo["emergency-contact"]["phone"])

				// Next step
				cy.get('.booking-footer > div > .btn').click()

				// Check checkboxes
				cy.get('.checkbox-option').click({multiple: true})
				cy.contains('Ja, de gegevens kloppen').click()

				cy.get('[data-cy=price-tag] > .price-block__amount').then((price) => {
					const finalPrice = parseFloat(price.text())
					expect(finalPrice).to.eq(checkOutPrice)
				})
			})
		})
	}
);
